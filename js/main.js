
$(".btn_modal").fancybox({
    'padding'    : 0
});



// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


// rating

$('.raty').raty({
    number: 5,
    starHalf      : 'fa fa-star-half-o',
    starOff       : 'fa fa-star-o',
    starOn        : 'fa fa-star',
    cancelOff     : 'fa fa-minus-square',
    cancelOn      : 'fa fa-check-square',
    score: function() {
        return $(this).attr('data-score');
    },
    readOnly: function() {
        return $(this).attr('data-readOnly');
    },
});

(function() {

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.page').toggleClass('open');
    });

}());

(function() {

    $('.search_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.search__toggle').toggleClass('open');
        $('.search__content').slideToggle('fast');
    });

}());

(function() {

    $('.section__header').on('click', function(e){
        e.preventDefault();
        $(this).closest('.section').toggleClass('close');
        $(this).closest('.section').find('.section__content').slideToggle('fast');
    });

}());




